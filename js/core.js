

var config = {
    apiKey: "AIzaSyCzf8hUH6nh0JC8pTz3AYmZN1ZGCLQ3Zq0",
    authDomain: "chamaangular.firebaseapp.com",
    databaseURL: "https://chamaangular.firebaseio.com",
    projectId: "chamaangular",
    storageBucket: "chamaangular.appspot.com",
    messagingSenderId: "396372807247"
};
firebase = firebase.initializeApp(config);
db = firebase.firestore();
listClientes = [];
listCupones = [];
contadorCupones=0;
db.collection("clientes").onSnapshot(function (querySnapshot) {
    var listado = [];
    querySnapshot.forEach(function (doc) {
        listado.push({
            id: doc.id,
            nombre: doc.data().nombre,
            apellido: doc.data().apellido,
            rut: doc.data().rut,
            fecha: doc.data().fecha,
            hora: doc.data().hora,
            clasificacion: doc.data().clasificacion
        });
    });
    listClientes = listado;
})

db.collection("cupones")
    .onSnapshot(function (queryCup) {
        var listCup = [];
        contadorCupones=0;
        queryCup.forEach(function (docCup) {
            contadorCupones++;
            listCup.push({
                id: docCup.id,
                fecha: docCup.data().fecha,
                hora: docCup.data().hora,
                rut: docCup.data().rut,
                nombre: docCup.data().nombre,
                apellido: docCup.data().apellido,
            }
            );
        });

        listCupones = listCup;
    });


function solicitudCupon(rut) {
    var rut = rut.replace(".", "");
    var rut = rut.replace(".", "");
    var rut = rut.replace(".", "");
    validarCliente(rut);
}

function actualizarClientes(list) {
    listClientes = list
}


function iniciarCupones(listCup) {
    listCup = [];
}
function entregarClientes() {
    return listClientes;
}
function entregarCupones() {
    return listCupones;
}

function registrarCliente(rut, nombre, apellido) {
    if (entregarClientes().filter(function (el) {
        return el.rut == rut
    }).length < 1) {
        db.collection("clientes").add({
            rut: rut,
            nombre: nombre,
            apellido: apellido,
            fecha: Date.now()
        })
    } else {
        console.log('uste esta haciendo trampa');
    }
}
function validarCliente(rut) {
    listadoFiltroRut = entregarClientes().filter(function (el) {
        return el.rut == rut
    });
    if (listadoFiltroRut.length < 1) {
        console.log("Se abre la modal de aviso");
        Swal.fire({
            title: 'Rut no encontrado',
            text: "Desea volver a intentarlo?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si!',
            
        }).then((result) => {
            if (result.value) {
            
            }else{
                $('#rut').val('')
            }
          })
    } else {
        var menosDeCuatro = validarMaxCupones(listadoFiltroRut[0]);
        if (menosDeCuatro) {
            console.log('menos de 4 cupones');
            validarHora(listadoFiltroRut[0]);
        } else {
            Swal.fire({
                title: 'Maximo de Cupones',
                text: "Usted ya sacó el maximo de cupones ",
                type: 'warning',
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ok'
            })
        }
    }
}
function imprimirCupon(cliente) {
    $('#nombreCupon').html(cliente.nombre+" "+cliente.apellido)

    $('#rutCupon').html(cliente.rut)
    $('#horaCupon').html(obtenerHora());
    $('#fechaCupon').html(obtenerFecha());
    $('#horaSorteo').html(obtenerHora());
    $('#fechaSorteo').html(obtenerFecha());
    console.log("La clasificacion del cliente es: " + cliente.clasificacion)
    switch (cliente.clasificacion) {
        case 'Bronce':
            $('#montoCupon').html('$3.000')
            $('#clasificacionCupon').html("Silver");
            break;
        case 'Plata':
            $('#montoCupon').html('$5.000');
            $('#clasificacionCupon').html("Golden");
            break;
        case 'Oro':
            $('#montoCupon').html('$10.000');
            $('#clasificacionCupon').html("Platinium");
            break;
        default:
            break;
    }
    var impresion = $('#divSorteo').html();
    var nuevaVemtama = window.open('', 'Print-Window');
    nuevaVemtama.document.open();
    nuevaVemtama.document.write('<html><body onload="window.print()">' + impresion + '</body></html>');
    nuevaVemtama.document.close();
    setTimeout(function () {
        nuevaVemtama.close();
        if (calcularHoraEnMin(obtenerHora()) > 1199) {
            console.log("abriendo sorteo")
            $('#nombreSorteo').html(cliente.nombre+" "+cliente.apellido)
            $('#rutSorteo').html(cliente.rut)
            $('#contadorCupon').html("N°"+contadorCupones);
            var divToPrint = $('#divImpresion').html();
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><body onload="window.print()">' + divToPrint + '</body></html>');
            newWin.document.close();
            setTimeout(function () { newWin.close(); }, 10);
        }
    }, 10);

}


function generarCupon(cliente) {
    db.collection("cupones").add({
        rut: cliente.rut,
        nombre: cliente.nombre,
        apellido: cliente.apellido,
        fecha: obtenerFecha(),
        hora: obtenerHora()
    }).then(function () {



    })
    $('#rut').val("");
    imprimirCupon(cliente);
    var clienteActual = db.collection("clientes").doc(cliente.id);
    return clienteActual.update({
        hora: obtenerHora(),
        fecha: obtenerFecha()
    })

}


function validarMaxCupones(cliente) {
    if (entregarCupones().filter(function (el) {
        return el.fecha == obtenerFecha() && el.rut == cliente.rut
    }).length < 2) {
        return true;
    } else {

        return false
    }
}
function validarHora(cliente) {
    console.log(cliente)
    nuevoArreglo = entregarClientes().filter(function (el) {
        return el.fecha == obtenerFecha() && el.rut == cliente.rut
    })
    if (nuevoArreglo.length < 1) {
        console.log("El cliente : " + cliente.nombre + "No  tiene cupon hoy")
        generarCupon(cliente)

    } else {

        nuevoArregloHora = entregarCupones().filter(function (el) {
            return el.fecha == obtenerFecha() && el.rut == cliente.rut
        })
        acumuladorAntesOcho = 0;
        acumuladorDespuesOcho = 0;
        for (let index = 0; index < nuevoArregloHora.length; index++) {
            if (calcularHoraEnMin(nuevoArregloHora[index].hora) < 1199) {
                acumuladorAntesOcho++;
            } else {
                acumuladorDespuesOcho++;
            };
        }

        console.log("antes de las 8" + acumuladorAntesOcho);
        console.log("despues de las 8" + acumuladorDespuesOcho)
        if (calcularHoraEnMin(obtenerHora()) < 1199) {
            console.log("son antes de las 8")
            if (acumuladorAntesOcho < 1) {
                console.log("El cliente : " + cliente.nombre + "Si tiene cupon hoy")
                //pendiente por validar hora
                var horaCliente = calcularHoraEnMin(cliente.hora);
                var horaActual = obtenerHora();
                horaActual = calcularHoraEnMin(obtenerHora());
                var difMinutos = horaActual - horaCliente;
                if (difMinutos > 60) {
                    console.log('1.' + difMinutos)
                    generarCupon(cliente);

                } else {
                    console.log('2.' + difMinutos)
                    var minFaltantes = 60 - difMinutos
                    Swal.fire({
                        title: 'Debe Esperar para volver a sacar otro cupon',
                        text: "Minutos Pendiente: " + minFaltantes,
                        type: 'warning',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ok'
                    })

                }

            } else {
                Swal.fire({
                    title: 'Debe Esperar hasta las 20:00',
                    text: "Vuelva despues de las 20:00",
                    type: 'warning',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                })
            }
        } else {
            if (acumuladorDespuesOcho < 1) {
                console.log("son despues de las 8")
                console.log("El cliente : " + cliente.nombre + "Si tiene cupon hoy")
                //pendiente por validar hora
                var horaCliente = calcularHoraEnMin(cliente.hora);
                var horaActual = obtenerHora();
                horaActual = calcularHoraEnMin(obtenerHora());
                var difMinutos = horaActual - horaCliente;
                if (difMinutos > 60) {
                    console.log('1.' + difMinutos)
                    generarCupon(cliente);

                } else {
                    console.log('2.' + difMinutos)
                    var minFaltantes = 60 - difMinutos
                    Swal.fire({
                        title: 'Debe Esperar para volver a sacar otro cupon',
                        text: "Minutos Pendiente: " + minFaltantes,
                        type: 'warning',
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ok'
                    })

                }

            } else {
                Swal.fire({
                    title: 'Ya obtuvo el maximo de Cupones',
                    text: "Vuelva mañana",
                    type: 'warning',
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ok'
                })
            }
        }








    }
}
