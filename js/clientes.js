var Clientes = class {
    constructor(nombre, apellido, rut, horaCupon, clasificacion, fecha) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.rut = rut;
        this.horaCupon = horaCupon;
        this.clasificacion = clasificacion;
        this.fecha = fecha;
    }
};